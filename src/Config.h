#ifndef CONFIG_H
#define CONFIG_H

const int screenWidth = 405;
const int screenHeight = 810;
enum class TYPE { PLAY, OPTIONS, EXIT, RESUME, GOBACK, VOLUME, CREDITS,MORE,LESS,PLAYAGAIN};
#endif
