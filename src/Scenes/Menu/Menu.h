#pragma once
#ifndef MENU_H
#define MENU_H
#include "../Scene.h"
class Menu:public Scene {
private:
	Texture2D _background;
	Music _music;
public:
	void init();
	void update();
	void draw();
	void reinit();
	Music getMusic();
	void setMusic(Music music);
	void playMusic();
};
#endif // !MENU_H
