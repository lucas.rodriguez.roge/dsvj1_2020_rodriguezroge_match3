#ifndef OPTIONSMENU_H
#define OPTIONSMENU_H
#include "../Scene.h"
#include "../../Entities/Buttons/Buttons.h"
#include "../../Entities/Sound/Sound.h"
#include "../VolumeMenu/VolumeMenu.h"
const short cantbotonesoptions=3;
const short _canttextures = 2;
class OptionsMenu :public Scene {
private:
	Texture2D _alltextures[_canttextures];
	Buttons* _buttons[cantbotonesoptions];
	bool _exit;
public:
	void init();
	void update(short& score);
	void draw();
	void reinit();
	void play(short& score);
};
#endif // !OPTIONSMENU_H
