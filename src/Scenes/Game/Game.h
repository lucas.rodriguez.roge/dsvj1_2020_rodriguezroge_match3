#pragma once
#ifndef GAME_H
#define GAME_H

#include "../Scene.h"
#include "../../Config.h"
#include "../../Entities/Bottle/Bottle.h"
#include "../../Entities/Buttons/Buttons.h"
#include "../../Entities/Sound/Sound.h"
#include "raylib.h"
#include <list>
using namespace std;
const short width = 9;
const short height = 10;
const short jewelgap = 10;
const short canttextures = 7;
const short cantbuttonsgame = 2;
class Game :public Scene {
private:
	Buttons* _buttons[cantbuttonsgame];
	Texture2D _alltextures[canttextures];
	Bottle* _jewels[height][width];
	Rectangle _ui;
	bool _gameover;
	bool _flagfirstlick;
	bool _isinbottle;
	bool _pause;
	list <Bottle> _pos;
	short _miliseconds;
	short _seconds;
	short _minutes;
	short _score;
public:
	void init(short& score);
	void draw(short& score);
	void update(short& score);
	void reinit();
	void play(short& score);
	void ui(short score);
	void addBottle(short i, short j);
	bool isAdyacent(short i, short j);
	void assignType(short i,short j);
	bool isOnList(short i, short j);
};
#endif // !GAME_H
