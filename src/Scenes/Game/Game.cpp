#include "Game.h"

void Game::init(short & score) {
	_alltextures[0] = LoadTexture("res/Textures/bluebottle.png");
	_alltextures[1] = LoadTexture("res/Textures/redbottle.png");
	_alltextures[2] = LoadTexture("res/Textures/pinkbottle.png");
	_alltextures[3] = LoadTexture("res/Textures/violetbottle.png");
	_alltextures[4] = LoadTexture("res/Textures/yellowbottle.png");
	_alltextures[5] = LoadTexture("res/Textures/Textbox.png");
	_alltextures[6] = LoadTexture("res/Textures/pause.png");
	Texture2D _auxbackground=LoadTexture("res/Textures/gamebackground.png");
	Vector2 resumepos;
	Vector2 gobackpos;
	TYPE resume = TYPE::RESUME;
	TYPE goback = TYPE::GOBACK;
	PlayMusicStream(sound::musicgame);
	resumepos.x = screenWidth / 2 - (_alltextures[5].width / 2);
	resumepos.y = screenHeight / 3;
	gobackpos.x = screenWidth / 2 - (_alltextures[5].width / 2);
	gobackpos.y = screenHeight / 2;
	for (short i = 0; i < cantbuttonsgame; i++) {
		_buttons[i] = new Buttons();
	}
	_buttons[0]->init(resumepos, resume, _alltextures[5]);
	_buttons[1]->init(gobackpos, goback, _alltextures[5]);

	_ui.x = 0;
	_miliseconds = 0;
	_seconds = 0;
	_minutes = 2;
	_ui.y = screenHeight - 200;
	_ui.width = screenWidth;
	_ui.height = 400;
	setBackground(_auxbackground);
	for (short i = 0; i < height; i++) {
		for (short j = 0; j < width; j++) {
			_jewels[i][j] = new Bottle();
			assignType(i, j);
		}
	}
	_gameover = false;
	_flagfirstlick = false;
	_isinbottle = false;
	_pause = false;
}
void Game::draw(short & score) {
	DrawTexture(getBackground(),0,0,WHITE);
	ui(score);
	for (short i = 0; i < height; i++) {
		for (short j = 0; j < width; j++) {
			if (_jewels[i][j]!=NULL)
			DrawTexture(_jewels[i][j]->getTexture(), _jewels[i][j]->getBoxColliderX(), _jewels[i][j]->getBoxColliderY(), WHITE);
		}
	}
	list <Bottle> ::iterator it;
	for (it = _pos.begin(); it != _pos.end(); ++it) {
		DrawRectangleLinesEx(it->getBoxCollider(), 5, BLACK);
	}
	if (_pause) {
		for (short i = 0; i < cantbuttonsgame; i++) {
			_buttons[i]->draw();
		}
	}
}
void Game::update(short & score) {
	UpdateMusicStream(sound::musicgame);
	if (!_pause) {
		if (_miliseconds < 60) {
			_miliseconds++;

		}
		else {
			_miliseconds = 0;
			if (_seconds > 0) {
				_seconds--;
			}
			else {
				_seconds = 59;
				_minutes--;
			}
		}
		for (short i = 0; i < height; i++) {
			for (short j = 0; j < width; j++) {
				if (IsMouseButtonDown(MOUSE_LEFT_BUTTON)) {
					if (_jewels[i][j] != NULL) {
						if (CheckCollisionPointRec(GetMousePosition(), _jewels[i][j]->getBoxCollider())) {
							if (!_flagfirstlick) {
								_pos.assign(1, *_jewels[i][j]);
								_flagfirstlick = true;
							}
							if (isAdyacent(i, j) && (_pos.front().getType() == _jewels[i][j]->getType())) {
								if (!isOnList(i,j))
								addBottle(i, j);
							}
						}
					}
				}
				else if (_pos.size() > 2) {
					PlaySound(sound::blockcomplete);
					list <Bottle> ::iterator it;
					for (it = _pos.begin(); it != _pos.end(); ++it) {
						for (short i = 0; i < height; i++) {
							for (short j = 0; j < width; j++) {
								if (_jewels[i][j] != NULL) {
									if (_jewels[i][j]->getBoxColliderX() == it->getBoxColliderX() && _jewels[i][j]->getBoxColliderY() == it->getBoxColliderY()) {
										delete _jewels[i][j];
										_jewels[i][j] = NULL;
										_jewels[i][j] = new Bottle();
										assignType(i, j);
									}
								}
							}
						}
					}
					switch(_pos.size()) {
					case 3:
						score += 150;
						break;
					case 4:
						score += 200;
						break;
					case 5:
						score += 250;
						break;
					case 6:
						score += 300;
						break;
					case 7:
						score += 350;
						break;
					case 8:
						score += 400;
						break;
					case 9:
						score += 500;
						break;
					case 10:
						score += 600;
						break;
					case 11:
						score += 700;
						break;
					case 12:
						score += 800;
						break;
					case 13:
						score += 900;
						break;
					case 14:
						score += 1000;
						break;
					case 15:
						score += 1500;
						break;
					}
					_pos.clear();
					_flagfirstlick = false;
				}
				if (IsMouseButtonReleased(MOUSE_LEFT_BUTTON)){
					_pos.clear();
					_flagfirstlick = false;
				}
			}
		}
		if (CheckCollisionPointRec(GetMousePosition(), { static_cast<float>(screenWidth / 2 - _alltextures[6].width / 2),static_cast<float>(screenHeight - 50),static_cast<float>(_alltextures[6].width),static_cast<float>(_alltextures[6].height) }) && IsMouseButtonReleased(MOUSE_LEFT_BUTTON)) { // pause button
			_pause = true;
		}
		if (_seconds == 0 && _minutes == 0) {
			_gameover = true;
		}
	}
	else {
		for (short i = 0; i < cantbuttonsgame; i++) {
			_buttons[i]->buttonManagment();
		}
		if (CheckCollisionPointRec(GetMousePosition(), _buttons[0]->getBoxCollider()) && (IsMouseButtonReleased(MOUSE_LEFT_BUTTON))) {
			_pause = false;
		}
		if (CheckCollisionPointRec(GetMousePosition(), _buttons[1]->getBoxCollider()) && (IsMouseButtonReleased(MOUSE_LEFT_BUTTON))) {
			score = 0;
			_gameover = true;
		}
	}
}
void Game::reinit() {
	for (short i = 0; i < height; i++) {
		for (short j = 0; j < width; j++) {
			if (_jewels[i][j] != NULL) {
				_jewels[i][j] = NULL;
				delete _jewels[i][j];
			}
		}
	}
	for (short i = 0; i < canttextures; i++) {
		UnloadTexture(_alltextures[i]);
	}
}
void Game::play(short & score) {
	init(score);
	while(!_gameover) {
		BeginDrawing();
		ClearBackground(BLACK);
		draw(score);
		update(score);
		EndDrawing();
	}
	reinit();
}
void Game::addBottle(short i,short j) {
	_pos.push_front(*_jewels[i][j]);
}
bool Game::isAdyacent(short i, short j){
	Vector2 minvalues{0,0};
	Vector2 maxvalues{0,0};
	if (_pos.front().getBoxColliderY() == 0) {
		minvalues.y = _pos.front().getBoxColliderY(); //UP
		maxvalues.y = _pos.front().getBoxColliderY() + (_pos.front().getBoxColliderHeight() * 2) + jewelgap;
	}
	else if (_pos.front().getBoxColliderY() + _pos.front().getBoxColliderHeight() == _jewels[height - 1][j]->getBoxColliderY() + _jewels[height - 1][j]->getBoxColliderHeight()) {
		maxvalues.y = _pos.front().getBoxColliderY() + _pos.front().getBoxColliderHeight();//DOWN
		minvalues.y = _pos.front().getBoxColliderY() - _pos.front().getBoxColliderHeight() - jewelgap;
	}
	else {
		minvalues.y = _pos.front().getBoxColliderY() - _pos.front().getBoxColliderHeight() - jewelgap;
		maxvalues.y = _pos.front().getBoxColliderY() + (_pos.front().getBoxColliderHeight()*2) + jewelgap;
	}
	if (_pos.front().getBoxColliderX() == 0) {
		minvalues.x = _pos.front().getBoxColliderX();//LEFT
		maxvalues.x = _pos.front().getBoxColliderX() + (_pos.front().getBoxColliderWidth() * 2) + jewelgap;
	}
	else if (_pos.front().getBoxColliderX() + _pos.front().getBoxColliderWidth() == _jewels[i][width - 1]->getBoxColliderX() + _jewels[i][width - 1]->getBoxColliderWidth()) {
		maxvalues.x = _pos.front().getBoxColliderY() + _pos.front().getBoxColliderWidth();//RIGHT
		minvalues.x = _pos.front().getBoxColliderX() - _pos.front().getBoxColliderWidth() - jewelgap;
	}
	else {
		minvalues.x = _pos.front().getBoxColliderX() - _pos.front().getBoxColliderWidth() - jewelgap;
		maxvalues.x = _pos.front().getBoxColliderX() + (_pos.front().getBoxColliderWidth()*2) + jewelgap;
	}
	bool aux = ((GetMouseX() >= minvalues.x) && (GetMouseX() <= maxvalues.x)) && ((GetMouseY() >= minvalues.y) && (GetMouseY() <= maxvalues.y));
	std::cout << aux;
	return ((GetMouseX() >= minvalues.x) && (GetMouseX() <= maxvalues.x)) && ((GetMouseY() >= minvalues.y) && (GetMouseY() <= maxvalues.y));
}
void Game::ui(short score) {
	DrawRectangleRec(_ui, BLACK);
	DrawText("Time Remaining", _ui.x+25, _ui.y+25, 20, WHITE);
	if (_minutes==1||_minutes==2) {
		DrawText(FormatText("%d", _minutes), _ui.x + 45, _ui.y + 75, 40, WHITE);
		DrawText(":", _ui.x + 75, _ui.y + 75, 40, WHITE);
		DrawText(FormatText("%d", _seconds), _ui.x + 85, _ui.y + 75, 40, WHITE);
	}
	else if (_seconds > 30) {
		DrawText(FormatText("%d", _minutes), _ui.x + 45, _ui.y + 75, 40, LIGHTGRAY);
		DrawText(":", _ui.x + 75, _ui.y + 75, 40, LIGHTGRAY);
		DrawText(FormatText("%d", _seconds), _ui.x + 85, _ui.y + 75, 40, LIGHTGRAY);
	}
	else if ((_seconds <= 30&&_seconds>=10) && _minutes == 0) {
		DrawText(FormatText("%d", _minutes), _ui.x + 45, _ui.y + 75, 40, YELLOW);
		DrawText(":", _ui.x + 75, _ui.y + 75, 40, YELLOW);
		DrawText(FormatText("%d", _seconds), _ui.x + 85, _ui.y + 75, 40, YELLOW);
	}
	else if (_seconds < 10 && _minutes == 0) {
		DrawText(FormatText("%d", _minutes), _ui.x + 45, _ui.y + 75, 40, RED);
		DrawText(":", _ui.x + 75, _ui.y + 75, 40, RED);
		DrawText(FormatText("%d", _seconds), _ui.x + 85, _ui.y + 75, 40, RED);
	}
	DrawText("Score", screenWidth - 125, _ui.y + 25,20, WHITE);
	DrawText(FormatText("%d", score),screenWidth-100, _ui.y + 75, 40, WHITE);
	DrawTexture(_alltextures[6], screenWidth / 2-_alltextures[6].width/2, screenHeight - 50,WHITE);
	if (CheckCollisionPointRec(GetMousePosition(), { static_cast<float>(screenWidth / 2 - _alltextures[6].width / 2),static_cast<float>(screenHeight - 50),static_cast<float>(_alltextures[6].width),static_cast<float>(_alltextures[6].height) })) { // pause button
		DrawTexture(_alltextures[6], static_cast<float>(screenWidth / 2 - _alltextures[6].width / 2), static_cast<float>(screenHeight - 50), DARKGRAY);
	}
}
void Game::assignType(short i, short j) {
	TYPEBOTTLE auxrandom = static_cast<TYPEBOTTLE>(GetRandomValue(0, 4));
	_jewels[i][j]->setType(auxrandom);
	switch (auxrandom)
	{
	case TYPEBOTTLE::TYPEBLUE:
		_jewels[i][j]->setTexture(_alltextures[0]);
		break;
	case TYPEBOTTLE::TYPEPINK:
		_jewels[i][j]->setTexture(_alltextures[1]);
		break;
	case TYPEBOTTLE::TYPERED:
		_jewels[i][j]->setTexture(_alltextures[2]);
		break;
	case TYPEBOTTLE::TYPEVIOLET:
		_jewels[i][j]->setTexture(_alltextures[3]);
		break;
	case TYPEBOTTLE::TYPEYELLOW:
		_jewels[i][j]->setTexture(_alltextures[4]);
		break;
	}
	_jewels[i][j]->setBoxColliderHeight(20.0f);
	_jewels[i][j]->setBoxColliderWidth(40.0f);
	_jewels[i][j]->setBoxColliderX(j * (_jewels[i][j]->getBoxColliderWidth() + jewelgap));
	_jewels[i][j]->setBoxColliderY(i * (_jewels[i][j]->getBoxColliderHeight() + jewelgap));
	_jewels[i][j]->init({ j * (_jewels[i][j]->getBoxColliderWidth() + jewelgap - 5),i * (_jewels[i][j]->getBoxColliderHeight() + jewelgap + 30) });
}
bool Game::isOnList(short i,short j) {
	list <Bottle> ::iterator it;
	bool is = false;
	for (it = _pos.begin(); it != _pos.end(); ++it) {
		if (it->getBoxColliderX()==_jewels[i][j]->getBoxColliderX()&& it->getBoxColliderY() == _jewels[i][j]->getBoxColliderY()) {
			is = true;
			break;
		}
	}
	return is;
}