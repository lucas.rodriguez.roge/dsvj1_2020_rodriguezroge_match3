#include "VolumeMenu.h"
void VolumeMenu::init() {
	_alltextures[0] = LoadTexture("res/Textures/volumebackground.png");
	_alltextures[1] = LoadTexture("res/Textures/textbox.png");
	_alltextures[2] = LoadTexture("res/Textures/minitextbox.png");
	setBackground(_alltextures[0]);
	PlayMusicStream(sound::musicvolume);
	_volume.rec = { static_cast<float>(screenWidth / 2 - (_alltextures[1].width / 2)), static_cast<float>(screenHeight / 3), 40, 20 };
	_volume.color = BLUE;
	Vector2 morepos;
	Vector2 lesspos;
	Vector2 gobackpos;

	TYPE more = TYPE::MORE;
	TYPE less = TYPE::LESS;
	TYPE goback = TYPE::GOBACK;

	morepos.x = screenWidth / 2 + _alltextures[1].width/2;
	morepos.y = screenHeight / 2;
	lesspos.x = screenWidth / 2 - _alltextures[1].width;
	lesspos.y = screenHeight / 2;
	gobackpos.x = screenWidth / 2 - (_alltextures[1].width / 2);
	gobackpos.y = screenHeight - 100;
	for (short i = 0; i < cantbotonesvolume; i++) {
		_buttons[i] = new Buttons();
	}
	_buttons[0]->init(morepos, more, _alltextures[2]);
	_buttons[1]->init(lesspos, less, _alltextures[2]);
	_buttons[2]->init(gobackpos, goback, _alltextures[1]);
	_exit = false;
	input = 2;
}
void VolumeMenu::update(short& score) {
	UpdateMusicStream(sound::musicvolume);
	for (short i = 0; i < cantbotonesvolume; i++) {
		_buttons[i]->buttonManagment();
	}
	if (CheckCollisionPointRec(GetMousePosition(), _buttons[0]->getBoxCollider()) && (IsMouseButtonReleased(MOUSE_LEFT_BUTTON))) {

	}
	if (CheckCollisionPointRec(GetMousePosition(), _buttons[1]->getBoxCollider()) && (IsMouseButtonReleased(MOUSE_LEFT_BUTTON))) {

	}
	if (CheckCollisionPointRec(GetMousePosition(), _buttons[2]->getBoxCollider()) && (IsMouseButtonReleased(MOUSE_LEFT_BUTTON))) {
		_exit = true;
	}
}
void VolumeMenu::draw() {
	DrawTexture(getBackground(), 0, 0, WHITE);
	for (short i = 0; i < cantbotonesvolume; i++)
	{
		_buttons[i]->draw();
	}
	volumeSelection();
}
void VolumeMenu::reinit() {
	for (short i = 0; i < _canttexturesvolume; i++) {
		UnloadTexture(_alltextures[i]);
	}
}
void VolumeMenu::play(short& score) {
	init();
	while (!_exit) {
		BeginDrawing();
		ClearBackground(BLACK);
		update(score);
		draw();
		EndDrawing();
	}
	reinit();
}
void VolumeMenu::volumeSelection() {
	float auxvolumen[6]{ 0,0.05f,0.2f,0.3f,0.4f,0.5f };
	if (input < 5) {
		if (CheckCollisionPointRec(GetMousePosition(), _buttons[0]->getBoxCollider()) && (IsMouseButtonReleased(MOUSE_LEFT_BUTTON))) {
			input++;
		}
	}
	else input = 5;
	if (input > 0) {

		if (CheckCollisionPointRec(GetMousePosition(), _buttons[1]->getBoxCollider()) && (IsMouseButtonReleased(MOUSE_LEFT_BUTTON))) {
			input--;
		}		
	}
	else input = 0;
	for (short i = 0; i < 5; i++) {
		if (input > i) {
			DrawRectangle(_volume.rec.x + (i * 50), _volume.rec.y, _volume.rec.width, _volume.rec.height, _volume.color);
			DrawRectangleLinesEx({ _volume.rec.x + (i * 50), _volume.rec.y, _volume.rec.width, _volume.rec.height }, 4, GOLD);
		}
	}
	sound::modificarVolumen(auxvolumen, input);

}