#pragma once
#ifndef VOLUMEMENU_H
#define VOLUMEMENU_H
#include "raylib.h"
#include "../Scene.h"
#include "../../Entities/Buttons/Buttons.h"
#include "../../Entities/Sound/Sound.h"
const short cantbotonesvolume = 3;
const short _canttexturesvolume = 3;
struct volume {
	Rectangle rec;
	Color color;
};
class VolumeMenu :public Scene {
private:
	Texture2D _alltextures[_canttexturesvolume];
	Buttons* _buttons[cantbotonesvolume];
	volume _volume;
	bool _exit;
	short input;
public:
	void init();
	void update(short& score);
	void draw();
	void reinit();
	void play(short& score);
	void volumeSelection();
};
#endif // !VOLUMEMENU_H
