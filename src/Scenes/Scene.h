#pragma once
#ifndef SCENE_H
#define SCENE_H
#include "raylib.h"
#include <iostream>
using namespace std;
class Scene {
private:
	Texture2D _background;
	Music _music;
public:
	virtual void init() { cout << "cant init scene"<<endl; }
	virtual void update() { cout << "cant update scene" << endl; }
	virtual void draw() { cout << "cant draw scene" << endl; }
	virtual void reinit() { cout << "cant deinitialize scene" << endl; }
	virtual void play(short & score)=0;
	virtual void setBackground(Texture2D background) { _background = background; }
	virtual Texture2D getBackground() { return _background; }
};
#endif // !SCENE_H
