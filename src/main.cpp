#include "raylib.h"
#include "Config.h"
#include "Scenes/Game/Game.h"
#include "Scenes/MainMenu/MainMenu.h"

int main(void)
{
    // Initialization
    //--------------------------------------------------------------------------------------

    InitWindow(screenWidth, screenHeight, "BottleMatch");
    SetTargetFPS(60);               // Set our game to run at 60 frames-per-second
    InitAudioDevice();
    MainMenu* mainmenu = new MainMenu();
    mainmenu->play();
    delete mainmenu;
    CloseAudioDevice();
    CloseWindow();        // Close window and OpenGL context
    return 0;
}