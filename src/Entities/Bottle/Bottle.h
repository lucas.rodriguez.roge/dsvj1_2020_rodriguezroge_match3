#pragma once
#ifndef BOTTLE_H
#define BOTTLE_H
#include "raylib.h"
enum class TYPEBOTTLE{TYPEPINK,TYPERED,TYPEBLUE,TYPEYELLOW,TYPEVIOLET};
class Bottle {
private:
	TYPEBOTTLE _type;
	Texture2D _texture;
	Rectangle _boxcollider;
public:
	void init(Vector2 pos);
	void update();
	void draw();
	void reinit();
	TYPEBOTTLE getType();
	void setType(TYPEBOTTLE type);
	Rectangle getBoxCollider();
	Texture2D getTexture();
	void setTexture(Texture2D texture);
	void setBoxCollider(Rectangle boxcollider);
	void setBoxColliderX(float x);
	void setBoxColliderY(float y);
	void setBoxColliderWidth(float width);
	void setBoxColliderHeight(float height);
	float getBoxColliderX();
	float getBoxColliderY();
	float getBoxColliderWidth();
	float getBoxColliderHeight();
};
#endif // !JEWELS_H
