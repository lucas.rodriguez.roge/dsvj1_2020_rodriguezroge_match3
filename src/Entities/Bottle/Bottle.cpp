#include "Bottle.h"

void Bottle::init(Vector2 pos) {
	_boxcollider.x = pos.x;
	_boxcollider.y = pos.y;
	_boxcollider.width = _texture.width;
	_boxcollider.height = _texture.height;
}
void Bottle::update() {

}
void Bottle::draw() {
	
}
void Bottle::reinit() {

}
TYPEBOTTLE Bottle::getType() {
	return _type;
}
void Bottle::setType(TYPEBOTTLE type) {
	_type = type;
}
Rectangle Bottle::getBoxCollider() {
	return _boxcollider;
}
Texture2D Bottle::getTexture() {
	return _texture;
}
void Bottle::setTexture(Texture2D texture) {
	_texture = texture;
}
void Bottle::setBoxCollider(Rectangle boxcollider) {
	_boxcollider = boxcollider;
}
void Bottle::setBoxColliderX(float x) {
	_boxcollider.x = x;
}
void Bottle::setBoxColliderY(float y) {
	_boxcollider.y = y;
}
void Bottle::setBoxColliderWidth(float width) {
	_boxcollider.width = width;
}
void Bottle::setBoxColliderHeight(float height) {
	_boxcollider.height = height;
}
float Bottle::getBoxColliderX() {
	return _boxcollider.x;
}
float Bottle::getBoxColliderY() {
	return _boxcollider.y;
}
float Bottle::getBoxColliderWidth() {
	return _boxcollider.width;
}
float Bottle::getBoxColliderHeight() {
	return _boxcollider.height;
}