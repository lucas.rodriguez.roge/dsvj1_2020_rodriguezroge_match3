#pragma once
#ifndef SOUND_H
#define SOUND_H
#include "raylib.h"


namespace sound {
	extern Sound selectfirst;
	extern Sound blockwrong;
	extern Sound blockbeep;
	extern Sound blockcomplete;
	extern Music musicmenu;
	extern Music musicgame;
	extern Music musicoptions;
	extern Music musicvolume;

	extern void initSounds();
	extern void modificarVolumen(float auxvolumen[], short input);
}
#endif // !SOUND_H